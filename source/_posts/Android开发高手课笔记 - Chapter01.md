---
title: Android开发高手课笔记 - Chapter01
date: 2019-09-26 14:03:12
tags:
- Android
- Android学习
categories: Android开发高手课笔记
---

Android开发高手课 第一节 课后作业解析 —— 分析 Native crash dump

<!---more--->

## 问题一：使用demo中的minidump_stackwalk编译Native Crash 的dmp文件的时候，提示缺少so库链接：

**`minidump_stackwalk` 是breakpad官方的一个解析Native Crash的工具**，clone 官方breakpad项目，按照官方文档中的步骤，在breakpad下使用 `./configure && make` 编译整个项目，会生成不同环境下的`minicamp_stackwalk` 工具，使用这个工具，运行命令 `minicamp_stackwalk crashDump/xxxx.dmp >crashLog.txt`。会生成一个Native Crash的Log文件，名称为crashLog。文件内容部分如下：

```verilog
Operating system: Android
                  0.0.0 Linux 4.9.148 #1 SMP PREEMPT Sat Aug 10 01:41:27 CST 2019 aarch64
CPU: arm64
     8 CPUs

GPU: UNKNOWN

Crash reason:  SIGSEGV /SEGV_MAPERR
Crash address: 0x0
Process uptime: not available

Thread 0 (crashed)
 0  libcrash-lib.so + 0x5a0   //发生crash的位置以及寄存器信息
     x0 = 0x00000079f78c5460    x1 = 0x0000007fe1335974 
     x2 = 0x0000007fe1335a10    x3 = 0x00000079f73e1f98
     x4 = 0x0000007fe1335c30    x5 = 0x00000079f3b8d218
     x6 = 0x0000007fe13357c0    x7 = 0x00000079dc2f59b0
     x8 = 0x0000000000000001    x9 = 0x0000000000000000
    x10 = 0x0000000000430000   x11 = 0x00000079f77b66d8
    x12 = 0x0000007a7dc635d0   x13 = 0xd35dd79459140f0d
    x14 = 0x0000007a7d890000   x15 = 0xffffffffffffffff
    x16 = 0x00000079dc0b3fe8   x17 = 0x00000079dc0a358c
    x18 = 0x0000000000000001   x19 = 0x00000079f7815c00
    x20 = 0x0000000000000000   x21 = 0x00000079f7815c00
    x22 = 0x0000007fe1335c40   x23 = 0x00000079dc3685fb
    x24 = 0x0000000000000004   x25 = 0x0000007a7dfff5e0
    x26 = 0x00000079f7815ca0   x27 = 0x0000000000000001
    x28 = 0x0000007fe1335970    fp = 0x0000007fe1335940
     lr = 0x00000079dc0a35c4    sp = 0x0000007fe1335920
     pc = 0x00000079dc0a35a0
    Found by: given as instruction pointer in context
```



可以看到堆栈信息全部打印出来了，下一步使用ndk提供的addr工具进行符号解析。

## 问题二：使用ndk工具对crash 日志进行符号解析

在`ndk/toolchains/` 路径下有对应的不一样arm结构的解析工具，其中`arm-xxxx` 对应的是arm架构，而 `aarch64-xxxx` 对应的是arm64架构，我们这里上面有CPU输出，是arm64架构，所以这里应该使用`aarch64-xxx`，使用命令 `aarch64-linux-android-addr2line -f -C -e sample/build/intermediates/transforms/mergeJniLibs/debug/0/lib/arm64-v8a/libcrash-lib.so 0x5a0` 就会得到如下输出：

```verilog
Crash()
/Users/fengbo/project/AndroidAdvanceWithGeektime/Chapter01/sample/src/main/cpp/crash.cpp:10
```

 提示crash.cpp的第10行发生了错误。

命令的最后的 0x5a0 实际上是偏移量，通过ndk的工具解析后就可以得到发生错误的位置。

### 遇到的问题：

- `aarch64-linux-android-addr2line` 命令提示：commond not found。

  - 配置环境变量，Mac在.bash_profile下，配置: 

  ```
  export PATH=${PATH}:/Users/fengbo/myFiles/android-ndk-r13b/toolchains/aarch64-linux-android-4.9/prebuilt/darwin-x86_64/bin
  ```

  其中 `aarch64-xxx` 根据需要配置

- 运行命令发现提示 `file format not recognized addr2line`:

  - 没有运行对应的架构下的工具，例如在arm64架构下运行了`arm-xxxx`, 更换工具就可以解决。