---
title: ARTS - 2020.06.08 ~ 2020.06.14
date: 2020-06-14 17:32:07
tags:
- ARTS 打卡
categories: ARTS 打卡
---

ARTS打卡 - 2

<!---more--->

## 1. Algorithm

[题目：8. String to Integer (atoi)](https://leetcode.com/problems/string-to-integer-atoi/)

这次做的是一道字符串转int的题，刚看到题目的时候觉得比较简单，就是按顺序遍历字符串，如果找到数字就和之前的相加就可以了。但是在写的过程中，发现实际上有一些边界条件需要考虑，感觉以后在做题的时候可以先列出一些需要处理的特殊情况，比如一些边界条件。如果这道题按照 “【去空格】-> 【判断符号】->【求和并判断是否越界】” 这样的顺序的话，实际上也可以很顺利地写完。另外accept之后看了一下别人的解法，发现一个没有想到的点: 如果要计算数字，实际上可以通过 `int a = char - '0'`,  我在代码里是通过 ` >= 48 && <=57` 来判断的，显然前者更巧妙一些。

## 2. Review

[Android MVI with Kotlin Coroutines & Flow](https://quickbirdstudios.com/blog/android-mvi-kotlin-coroutines-flow/)

![](http://ww1.sinaimg.cn/large/795d6b61ly1gfrrzdukh8j20nd0gpq2w.jpg)

文章分享了一个用`kotln coroutines (actor + channel) `和`Stateflow`代替`RxJava`实现MVI的方法，上面的图就是MVI的工作流程，我的理解是相对于MVVM来说，在ViewModel里多出了一个state的概念，view可以根据这个state的状态变化而改变。Intent可以看作是Action，通过view来发出一个Intent去改变state的状态，通过这种方式，Intent与State就对应了起来。这样一来会产生一个问题是如何管理多个state，MVI不是一个新的模型，现在的MVI由于响应式的结构（observable/subscriber），都会使用RxJava去管理并响应状态。文章中提出了使用Kotlin中的`coroutines`以及`StateFlow` 来管理intent以及state，也就是说使用channle作为管道来发送Intent以及处理state， 这样做的好处是不仅可以与viewmodel的生命周期绑定，而且是线程安全的。此外，管理state可以使用`StateFlow`（stateflow我的理解是一个状态流，可能存在Intent的响应是需要多个state来完成）。StateFlow官方也提供了实现（在kotlin*1.3.3*可以作为*Experimental*使用），文章提出了三点使用StateFlow的好处：

- 改变状态不再需要*suspend*方法
- `StateFlow`包含了`Flow`的所有好处
- 当没有subscriber的时候，不会发送改变状态。

## 3. Tip

Jenkins 通过 ssh remote 执行shell 命令的时候出现访问钥匙串权限不足的情况，想通过shell脚本直接在命令行模拟输入用户名密码验证权限，学习到了expect脚本的用法，可以模拟命令行输入

http://xstarcd.github.io/wiki/shell/expect.html

## 4. Share

[“不科学”的“四舍五入” ？](https://www.ituring.com.cn/article/35304)

在使用python的`round()` 函数的时候，了解了一个叫做 *银行家舍入* 的做法，比如如下函数：

```python
print(round(0.946, 2))
print(round(0.943, 2))
print(round(0.945, 2))
print(round(0.935, 2))
```

上面的函数会输出如下结果：

```verilog
0.95
0.93
0.94
0.94
```

前两个输出我并没有感到以外，可以看到后两个输出怎么和我学过的四舍五入不一样，查了一下才知道*银行家舍入*的做法，大概就是如果舍入位为5的时候，则遵循 “奇进偶舍” 的原则。当舍入位不为5的时候，还是会遵守 “四舍五入” 。(如果一定要实现四舍五入可以通过Decimal模块，并指定舍入方式为 `ROUND_HALF_UP` )

