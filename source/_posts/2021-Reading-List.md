---
title: 2021-Reading-List
date: 2021-09-01 20:57:00
tags: 
- Reading
categories: Reading

---

2021 Reading List

<!---more--->

# 2021 Reading LIst
<br/>

<span style='display:inline-block;color:#ffffff;background:#42526D;padding:3px;'>**TODO**</span>  <span style='display:inline-block;color:#ffffff;background:#C9A5A7;padding:3px;'>**DOING**</span>   <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span>

| Name           | Author                | Link | Publishing/Release Date | Score /5 | Status                                                       | Summary | Type | classify |
| :------------- | :-------------------- | :--- | :---------------------- | :------- | :----------------------------------------------------------- | :------ | :--- | :------- |
| 莫失莫忘       | 石黑一雄              |      |                         | ⭐⭐⭐      | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 回归故里       | [法]迪迪埃·埃里蓬     |      |                         | ⭐⭐⭐      | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 杀死一只知更鸟 | [美] 哈珀·李          |      |                         | ⭐⭐⭐⭐⭐    | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 人生海海       | 麦加                  |      |                         | ⭐⭐⭐⭐⭐    | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 消失的13级台阶 | [日] 高野和明         |      |                         | ⭐⭐⭐⭐     | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 失落的卫星     | 失落的卫星            |      |                         | ⭐⭐⭐⭐⭐    | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 雪国           | 川端康成              |      |                         | ⭐⭐⭐      | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 鲁迅杂文集     | 鲁迅                  |      |                         | ⭐⭐⭐⭐     | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 我与地坛       | 史铁生                |      |                         | ⭐⭐⭐⭐     | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 增长黑客       | Sean Ellis            |      |                         |          | <span style='display:inline-block;color:#ffffff;background:#42526D;padding:3px;'>**TODO**</span> |         | Book | 商业     |
| 最好的告别     | 阿图 葛文德           |      |                         | ⭐⭐⭐⭐     | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 心理     |
| 沉默的病人     | [英]亚历克斯·麦克利兹 |      |                         | ⭐⭐⭐⭐     | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 逻辑思维       | 罗振宇                |      |                         | ⭐⭐⭐⭐     | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 思维方式 |
| 罗杰疑案       | 阿加莎 克里斯蒂       |      |                         | ⭐⭐⭐⭐⭐    | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 侦探     |
| 绝叫           | 叶真中显              |      |                         | ⭐⭐⭐⭐⭐    | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 侦探     |
| 狂人日记       | 鲁迅                  |      |                         | ⭐⭐⭐⭐     | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 文学     |
| 面纱           | 毛姆                  |      |                         |          | <span style='display:inline-block;color:#ffffff;background:#C9A5A7;padding:3px;'>**DOING**</span> |         | Book | 文学     |
| 从晚清到民国   | 唐德刚                |      |                         | ⭐⭐⭐⭐⭐    | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 历史     |
| 晚清之后是民国 | 赵焰                  |      |                         | ⭐⭐⭐⭐     | <span style='display:inline-block;color:#ffffff;background:#07865B;padding:3px;'>**DONE**</span> |         | Book | 历史     |